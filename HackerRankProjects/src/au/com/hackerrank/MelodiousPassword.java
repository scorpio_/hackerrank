/**
 * 
 */
package au.com.hackerrank;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

/**
 * @author vnayak
 *
 */
public class MelodiousPassword {

	private static final List<String> vowels = Arrays.asList(new String[] { "a"});//, "e", "i", "o", "u" });
	private static final List<String> consonants = Arrays.asList(new String[] { "b"});//, "c", "d", "f", "g", "h", "j", "k", "l", "m", "n", "p", "q", "r", "s", "t", "v", "w", "x", "z" });

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		List<String> result = new ArrayList<>();

		result.addAll(vowels);
		result.addAll(consonants);
		int n = 2;
		if (n == 1) {
			for (String str : vowels)
				System.out.println(str);
			for (String str : consonants)
				System.out.println(str);
		} else {
			System.out.println(combination(result, 2));
		}

	}

	private static void getPwdStartingVowel(int n, List<String> result) {
		
		
	}
	
	public static <T> List<List<T>> combination(List<T> values, int size) {

	    if (0 == size) {
	        return Collections.singletonList(Collections.<T> emptyList());
	    }

	    if (values.isEmpty()) {
	        return Collections.emptyList();
	    }

	    List<List<T>> combination = new LinkedList<List<T>>();

	    T actual = values.iterator().next();

	    List<T> subSet = new LinkedList<T>(values);
	    subSet.remove(actual);

	    List<List<T>> subSetCombination = combination(subSet, size - 1);

	    for (List<T> set : subSetCombination) {
	        List<T> newSet = new LinkedList<T>(set);
	        newSet.add(0, actual);
	        combination.add(newSet);
	    }

	    combination.addAll(combination(subSet, size));

	    return combination;
	}

}
