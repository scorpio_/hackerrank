/**
 * 
 */
package au.com.hackerrank;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * @author vnayak
 *
 */
public class ParseMsg {

	/**
	 * @param args
	 * @throws IOException
	 */
	public static void main(String[] args) throws IOException {
		String file1 = "./resource/msgs.txt";

		String file2 = "./resource/code.txt";

		File file = new File(file1);
		BufferedReader br = new BufferedReader(new FileReader(file));

		Map<String, String> msgs = new HashMap<>();
		String line;
		while ((line = br.readLine()) != null) {
			String[] temp = line.split("=");
			if (msgs.containsKey(temp[0]))
				System.out.println("Duplicate " + temp[0]);
			msgs.put(temp[0], temp[1]);

		}

		System.out.println("|Code                   |Error                   ||Description                |");
		System.out.println("|-----------------------|-----------------------|---------------------------|");

		br = new BufferedReader(new FileReader(file2));

		String line2;
		while ((line2 = br.readLine()) != null) {
			String[] temp = line2.split("=");

			String msg = msgs.get(temp[2]) != null ? msgs.get(temp[2]) : temp[2];
			System.out.println("|" + temp[1] + "|" + temp[0] + "|" + msg + "|");

		}

	}

}
