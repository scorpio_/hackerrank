/**
 * 
 */
package au.com.hackerrank;

import java.util.Scanner;

/**
 * @author vnayak
 *
 */
public class LoveForSix {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);

		int a = in.nextInt();
		int b = in.nextInt();

		if (a < 1 || a > 10 || b < 1 || b > 10)
			System.out.println("Hate");
		else
		if ((a + b) % 6 == 0 || (a - b) % 6 == 0)
			System.out.println("Love");
		else
			System.out.println("Hate");
	}

}
