/**
 * 
 */
package au.com.hackerrank;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

/**
 * @author vnayak
 *
 */
public class LibraryFine {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		String date1 = "31 8 2004";
		String date2 = "20 1 2004";
		
		DateFormat  format = new SimpleDateFormat("dd MM yyyy");
		try {
			Date dt1 = format.parse(date1);
			Date dt2 = format.parse(date2);
			BigDecimal tNumberOfDays = new BigDecimal( TimeUnit.DAYS.convert((dt1.getTime() - dt2.getTime()), TimeUnit.MILLISECONDS));
			System.out.println(TimeUnit.DAYS.convert((dt1.getTime() - dt2.getTime()), TimeUnit.MILLISECONDS));
			BigDecimal difference = tNumberOfDays.divide(new BigDecimal(31), RoundingMode.HALF_UP);
			
			System.out.println(tNumberOfDays);
			
			System.out.println(difference);
			
			if (difference.intValue() <= 0)
			{
				System.out.println("0");
			}
			else if (difference.intValue() > 12)
				System.out.println("10000");
			else if (difference.intValue() > 1)
				System.out.println(500 * difference.intValue());
			else
				System.out.println(15 * tNumberOfDays.intValue());

		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
