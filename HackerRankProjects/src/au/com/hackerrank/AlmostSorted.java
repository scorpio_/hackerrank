
/**
 * 
 */
package au.com.hackerrank;

import java.util.Arrays;
import java.util.Scanner;

/**
 * @author vnayak
 *
 */
public class AlmostSorted {

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int n = in.nextInt();
		int[] array = new int[n];
		for (int i = 0; i < n; i++) {
			array[i] = in.nextInt();
		}
		int[] check = checkSorted(array);
		// System.out.println(Arrays.toString(check));
		if (null == check)
			System.out.println("yes");
		else {

			if (trySwaping(array, check)) {
				System.out.println("yes");
				System.out.println("swap " + (check[0] + 1) + " " + (check[1] + 1));
			} else {
				// System.out.println("FUCK");
				tryReverse(array, check);
			}
		}

	}

	private static void tryReverse(int[] intAr, int[] indexes) {
		int lowerlimit = indexes[0];
		int upperlimit = indexes[1];

		int[] tempAr = Arrays.copyOfRange(intAr, lowerlimit, upperlimit + 1);

		int len = tempAr.length - 1;
		// reverse this array
		for (int i = 0; i < tempAr.length / 2; i++) {
			int temp = tempAr[i];
			tempAr[i] = tempAr[len];
			tempAr[len] = temp;

			if (len == 1)
				break;
			len--;
		}

		// System.out.println(Arrays.toString(tempAr));
		int j = 0;
		for (int i = 0; i < intAr.length; i++) {

			if (i == lowerlimit) {
				intAr[i] = tempAr[j];
				if (lowerlimit == upperlimit)
					break;
				lowerlimit++;
				j++;
			}

		}
		if (checkSorted(intAr) == null) {
			System.out.println("yes");
			System.out.println("reverse " + (indexes[0] + 1) + " " + (upperlimit + 1));
		} else {
			System.out.println("no");
		}
	}

	private static boolean trySwaping(int[] intAr, int[] indexes) {
		boolean result = false;

		int[] tempAr = new int[intAr.length];

		for (int i = 0; i < intAr.length; i++) {
			if (i == indexes[0]) {
				tempAr[i] = intAr[indexes[1]];
			} else if (i == indexes[1]) {
				tempAr[i] = intAr[indexes[0]];
			} else {
				tempAr[i] = intAr[i];
			}
		}

		if (null == checkSorted(tempAr))
			result = true;

		return result;
	}

	private static int[] checkSorted(int[] intAr) {
		int[] result = new int[2];

		int length = intAr.length;

		// find lower limit
		for (int i = 0; i < length; i++) {
			if (i != length - 1 && intAr[i] > intAr[i + 1]) {
				result[0] = i;
				break;
			}
		}

		// find upper limit
		for (int i = length - 1; i >= 0; i--) {
			if (i != 0 && intAr[i] < intAr[i - 1]) {
				result[1] = i;
				break;
			}
		}
		return result[0] != result[1] ? result : null;
	}

}
