/**
 * 
 */
package au.com.hackerrank;

import java.util.Scanner;

/**
 * @author Aakira
 *
 */
public class CompareTriplets {

    // /**
    // * @param args
    // */
    // public static void main(String[] args) {
    // int[] alice = { 5, 6, 7 };
    // int[] bob = { 3, 6, 10 };
    //
    // int aliceScore = 0, bobScore = 0;
    // for (int i = 0; i < alice.length; i++) {
    // if (alice[0] < bob[0]) {
    // ++bobScore;
    // } else if (alice[0] > bob[0]) {
    // ++aliceScore;
    // }
    // }
    // System.out.println(aliceScore+" "+bobScore);
    // }

    public static void main(String[] args) {
	Scanner in = new Scanner(System.in);
	int a0 = in.nextInt();
	int a1 = in.nextInt();
	int a2 = in.nextInt();
	int b0 = in.nextInt();
	int b1 = in.nextInt();
	int b2 = in.nextInt();

	int aliceScore = 0, bobScore = 0;
	if (a0 != b0 || a1 != b1 || a2 != b2) {
	    if (a0 > b0) {
		++aliceScore;
	    } else if (a0 < b0) {
		++bobScore;
	    }
	    if (a1 > b1) {
		++aliceScore;
	    } else if (a1 < b1) {
		++bobScore;
	    }
	    if (a2 > b2) {
		++aliceScore;
	    } else if (a2 < b2) {
		++bobScore;
	    }
	}

	System.out.println(aliceScore + " " + bobScore);

    }
}
