/**
 * 
 */
package au.com.hackerrank;

/**
 * @author vnayak
 *
 */
public class MinimumNumberString {

	private static final String MIN = "min";
	private static final String INT = "int";
	private static final String OPEN = "(";
	private static final String CLOSE = ")";
	private static final String COMMA_SPACE = ", ";

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		int i = 4;
		String result = "";

		int tempi = i - 2;
		result = getLast(result);

		if (tempi > 0) {
			for (int j = 0; j < tempi; j++) {
				result = encloseResult(result);
			}
		}

		System.out.println(result);

	}

	private static String encloseResult(String result) {
		return MIN.concat(OPEN).concat(INT).concat(COMMA_SPACE).concat(result).concat(CLOSE);
		
	}

	private static String getLast(String result) {
		return MIN.concat(OPEN).concat(INT).concat(COMMA_SPACE).concat(INT).concat(CLOSE);
	}

}
