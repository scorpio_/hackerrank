/**
 * 
 */
package au.com.hackerrank;

/**
 * @author vnayak
 *
 */
public class BreakingTheRecords {

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		int[] scores = new int[] { 0, 9, 3, 10, 2, 20 };
		int hig = 0;
		int temp = 0;
		for (int i = 0; i < scores.length; i++) {
			if (i == 0) {
				temp = scores[i];
				continue;
			}
			if (scores[i] > temp) {
				hig++;
				temp = scores[i];
			}

		}

		temp = 0;
		int low = 0;
		for (int i = 0; i < scores.length; i++) {
			if (i == 0) {
				temp = scores[i];
				continue;
			}
			if (temp > scores[i]) {
				low++;
				temp = scores[i];
			}

		}

		System.out.println(hig + " " + low);
	}

}
