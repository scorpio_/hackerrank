/**
 * 
 */
package au.com.hackerrank;

import java.util.Arrays;

/**
 * @author vnayak
 *
 */
public class LarryArray {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		int testcases = 3;
		int tse1 = 3;
		int[] tse1a = new int[] { 3, 1, 2 };
		int tse2 = 4;
		int[] tse2a = new int[] { 1, 3, 4, 2 };
		int tse3 = 5;
		int[] tse3a = new int[] { 1, 2, 3, 5, 4 };

		
		System.out.println(Arrays.toString(checkSorted(tse3a)));
		
	}

	private static int[] checkSorted(int[] intAr) {
		int[] result = new int[2];

		int length = intAr.length;

		// find lower limit
		for (int i = 0; i < length; i++) {
		    if (i != length - 1 && intAr[i] > intAr[i + 1]) {
			result[0] = i;
			break;
		    }
		}

		// find upper limit
		for (int i = length - 1; i >= 0; i--) {
		    if (i != 0 && intAr[i] < intAr[i - 1]) {
			result[1] = i;
			break;
		    }
		}
		return result[0] != result[1] ? result : null;
	    }
}
