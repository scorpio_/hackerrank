/**
 * 
 */
package au.com.hackerrank;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * @author vnayak
 *
 */
public class FunnyString {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int N = Integer.parseInt(in.nextLine());
		
		List<String> inputs = new ArrayList<>();
		
		for(int i =0; i<N;i++)
			inputs.add(in.nextLine());
		
		for(String str : inputs)
		{
			String rstr = new StringBuffer(str).reverse().toString();
			int len = str.length();
			boolean result = true;
			for(int i = 0; i < len-1; i++)
			{
				char a = str.charAt(i);
				char b = str.charAt(i+1);
				
				char z = rstr.charAt(i);
				char x = rstr.charAt(i+1);
				
				if(Math.abs(((int)a-(int)b))!=Math.abs(((int)z-(int)x)))
				{
					result = false;
					break;
				}
			}
			
			if(result)
				System.out.println("Funny");
			else
				System.out.println("Not Funny");
		}
		
//		System.out.println((int)s.charAt(0));
	}

}
