package au.com.hackerrank;
/**
 * 
 */

/**
 * @author vnayak
 *
 */
public class CandyBowl {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String[] nt = new String[] { "8", "5" };
		String[] c = new String[] { "3", "1", "7", "5", "3" };

		int[] cint = getIntArray(c);

		int n = Integer.parseInt(nt[0]);
		int t = Integer.parseInt(nt[1]);

		int total = 0;
		int temp = n;

		for (int i = 0; i < t; i++) {
			temp = temp - cint[i];
			if (temp < 5 && i + 1 != t) {
				total += n - temp;
				temp = n;
			}
		}

		System.out.println(total);

	}

	private static int[] getIntArray(String[] c) {
		int[] temp = new int[c.length];

		for (int i = 0; i < c.length; i++) {
			temp[i] = Integer.parseInt(c[i]);
		}

		return temp;
	}

}
