/**
 * 
 */
package au.com.hackerrank;

import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 * @author vnayak
 *
 */
public class PlusMinus {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		int n = 6;
		int[] array = new int[] { -4, 3, -9, 0, 4, 1 };

		int neg = 0;
		int pos = 0;
		int zero = 0;
		
		for(int i : array)
		{
			if(i == 0)
			{
				++zero;
			}
			else if(i>0)
			{
				++pos;
			}
			else
			{
				++neg;
			}
		}
		BigDecimal divi = new BigDecimal(n);
		BigDecimal res = new BigDecimal(pos);
		
		System.out.println(res.divide(divi, 6, RoundingMode.HALF_UP));
		
		res = new BigDecimal(neg);
		System.out.println(res.divide(divi, 6, RoundingMode.HALF_UP).setScale(6));
		
		res = new BigDecimal(zero);
		System.out.println(res.divide(divi,6, RoundingMode.HALF_UP).setScale(6));
	}

}
