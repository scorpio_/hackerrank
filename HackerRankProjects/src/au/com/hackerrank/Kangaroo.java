/**
 * 
 */
package au.com.hackerrank;

/**
 * @author vnayak
 *
 */
public class Kangaroo {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		int x1 = 7271;// 7271 2211 7915 2050
		int v1 = 2211;
		int x2 = 7915;
		int v2 = 2050;

		if (x1 < x2 && v1 > v2) {

			boolean result = false;
			while (true) {
				if (x1 == x2) {
					result = true;
					break;
				}

				x1 += v1;
				x2 += v2;
				if (x1 > x2) {
					break;
				}
			}

			if (result)
				System.out.println("YES");
			else
				System.out.println("NO");

		} else
			System.out.println("NO");

	}

}
