/**
 * 
 */
package au.com.hackerrank;

import java.io.ByteArrayInputStream;

/**
 * @author Vasu Nayak
 *
 */
public class TestCalculateBet {

    /**
     * @param args
     */
    public static void main(String[] args) {
	String[] temp1 = new String[] { "Bet:W:1:3", "Bet:W:2:4", "Bet:W:3:5", "Bet:W:4:5", "Bet:W:1:16", "Bet:W:2:8",
		"Bet:W:3:22", "Bet:W:4:57", "Bet:W:1:42", "Bet:W:2:98", "Bet:W:3:63", "Bet:W:4:15", "Bet:P:1:31",
		"Bet:P:2:89", "Bet:P:3:28", "Bet:P:4:72", "Bet:P:1:40", "Bet:P:2:16", "Bet:P:3:82", "Bet:P:4:52",
		"Bet:P:1:18", "Bet:P:2:74", "Bet:P:3:39", "Bet:P:4:105", "Result:2:3:1" };
	StringBuilder strBuilder = new StringBuilder();
	for (int i = 0; i < temp1.length; i++) {
	    strBuilder.append(temp1[i]);
	    if (i != temp1.length - 1) {
		strBuilder.append(System.getProperty("line.separator"));
	    }
	}
	ByteArrayInputStream in = new ByteArrayInputStream(strBuilder.toString().getBytes());
	System.setIn(in);

	CalculateBet.main(null);

	// optionally, reset System.in to its original
	System.setIn(System.in);

    }

}
