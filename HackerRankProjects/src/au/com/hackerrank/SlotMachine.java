package au.com.hackerrank;
import java.util.Arrays;

public class SlotMachine {

    public static void main(String[] args) {
	String[] in = new String[] {"1112", "1111", "1211", "1111"};//{ "137", "364", "115", "724" };
	
	int result = 0;

	int[] first = getIntArrayForindex(in, 0);

	for (int i = first.length - 1; i >= 0; i--) {
	    int temp = 0;
	    for (int i2 = in.length - 1; i2 != 0; i2--) {
		int[] next = getIntArrayForindex(in, i2);

		if (first[i] >= next[i]) {
		    temp = first[i];
		}
		else
		{
		    temp = next[i];
		}
	    }
	    result +=temp;
	}
	System.out.println(result);

    }

    private static int[] getIntArrayForindex(String[] in, int i) {
	String[] temp = in[i].split("");
	int[] intar = new int[temp.length];

	for (int i1 = 0; i1 < temp.length; i1++) {
	    intar[i1] = Integer.parseInt(temp[i1]);
	}

	Arrays.sort(intar);

	return intar;
    }

}
