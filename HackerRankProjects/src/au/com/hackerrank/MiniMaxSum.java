/**
 * 
 */
package au.com.hackerrank;

import java.math.BigInteger;
import java.util.Arrays;

/**
 * @author Aakira
 *
 */
public class MiniMaxSum {

    /**
     * @param args
     */
    public static void main(String[] args) {
	long a = 1;
	long b = 2;
	long c = 3;
	long d = 4;
	long e = 5;

	long[] array = new long[] { a, b, c, d, e };
	Arrays.sort(array);
	BigInteger sumMin = BigInteger.ZERO;
	BigInteger sumMax = BigInteger.ZERO;
	
	for(int i = 0; i < array.length-1; i++)
	{
	    sumMin = sumMin.add(BigInteger.valueOf(array[i]));
	}
	
	for(int i = array.length-1; i> 0 ;i--) {
	    sumMax = sumMax.add(BigInteger.valueOf(array[i]));
	}
	    
	System.out.println(sumMin+" "+sumMax);

    }

}
