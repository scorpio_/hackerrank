/**
 * 
 */
package au.com.hackerrank;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Scanner;
import java.util.Set;

/**
 * @author vnayak
 *
 */
public class Gemstones {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int N = Integer.parseInt(in.nextLine());

		List<String> inputs = new ArrayList<>();

		for (int i = 0; i < N; i++)
			inputs.add(in.nextLine());

		Set<String> cAr = getUniqueElements(inputs.get(0));

		int result = 0;
		for (String str : cAr) {
			boolean a = true;
			for (String str1 : inputs) {
				if (!str1.contains(str)) {
					a = false;
				}
			}
			if (a) {
				++result;
			}

		}
		System.out.println(result);

	}

	private static Set<String> getUniqueElements(String string) {
		Set<String> uni = new HashSet<>();

		for (int i = 0; i < string.length(); i++) {
			uni.add("" + string.charAt(i));
		}

		return uni;
	}
}
