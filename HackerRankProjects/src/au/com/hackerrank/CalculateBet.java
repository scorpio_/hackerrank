/**
 * 
 */
package au.com.hackerrank;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

/**
 * 
 * @author Vasu Nayak
 *
 */
public class CalculateBet {

    /**
     * Constant to hold the percentage thats taken by TABCORP for win.
     */
    private final static int WIN_PERCENT = 15;

    /**
     * Constant to hold the percentage thats taken by TABCORP for Plcae.
     */
    private final static int PLACE_PERCENT = 12;

    /**
     * @param args
     */
    public static void main(String[] args) {
	try (Scanner in = new Scanner(System.in)) {
	    List<String> input = new ArrayList<>();

	    // read until results are entered
	    while (in.hasNext()) {
		String read = in.nextLine();
		input.add(read);

		// break if results are entered.
		if (read.toUpperCase().startsWith("RESULT")) {
		    break;
		}
	    }
	    // process the inputs
	    processInput(input);
	} catch (NumberFormatException nfe) {
	    //
	    nfe.printStackTrace();
	}

    }

    /**
     * @param pInput
     */
    private static void processInput(final List<String> pInput) throws NumberFormatException {
	// separate winning bets from place bets.
	Map<String, Double> tWinBetsDetails = new HashMap<>();
	Map<String, Double> tPlaceBetsDetails = new HashMap<>();

	// Results place holder
	String[] tResults = new String[] {};
	Double tWinTotal = Double.valueOf("0.0");
	Double tPlaceTotal = Double.valueOf("0.0");

	for (String tInput : pInput) {
	    String[] tCurrentBetInput = tInput.split(":");
	    if (tCurrentBetInput[0].equalsIgnoreCase("result")) {
		tResults = tCurrentBetInput;
	    } else {
		String tBetType = tCurrentBetInput[1];
		String tHorse = tCurrentBetInput[2];

		Double tBetAmount = Double.valueOf(tCurrentBetInput[3]);

		if (tBetType.equals("W")) {
		    tWinTotal += tBetAmount;
		    if (tWinBetsDetails.containsKey(tHorse)) {
			tWinBetsDetails.put(tHorse, tBetAmount + tWinBetsDetails.get(tHorse));
		    } else {
			tWinBetsDetails.put(tHorse, tBetAmount);
		    }
		} else {
		    tPlaceTotal += tBetAmount;
		    if (tPlaceBetsDetails.containsKey(tHorse)) {
			tPlaceBetsDetails.put(tHorse, tBetAmount + tPlaceBetsDetails.get(tHorse));
		    } else {
			tPlaceBetsDetails.put(tHorse, tBetAmount);
		    }
		}
	    }
	}

	// Deduct TabCorp Commission
	tWinTotal = deductCommission(tWinTotal, WIN_PERCENT);
	tPlaceTotal = deductCommission(tPlaceTotal, PLACE_PERCENT);

	BigDecimal temp = BigDecimal.ZERO;
	for (int i = 1; i < tResults.length; i++) {

	    // win as well as place result needs to be processed for the
	    // first position.
	    if (i == 1) {
		// process win first.
		temp = new BigDecimal(tWinTotal / tWinBetsDetails.get(tResults[i]));
		temp = temp.setScale(2, RoundingMode.HALF_UP);
		System.out.println("Win:" + tResults[i] + ":$" + temp);

		// process place.
		temp = new BigDecimal((tPlaceTotal / 3) / tPlaceBetsDetails.get(tResults[i]));
		temp = temp.setScale(2, RoundingMode.HALF_UP);
		System.out.println("Place:" + tResults[i] + ":$" + temp);
	    } else {
		// process place.
		temp = new BigDecimal((tPlaceTotal / 3) / tPlaceBetsDetails.get(tResults[i]));
		temp = temp.setScale(2, RoundingMode.HALF_UP);
		System.out.println("Place:" + tResults[i] + ":$" + temp);
	    }
	}

    }

    private static Double deductCommission(final Double pTotal, final int pPercentage) {
	return pTotal - (pTotal * pPercentage / 100);
    }
}
