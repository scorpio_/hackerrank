/**
 * 
 */
package au.com.hackerrank;

/**
 * @author Aakira
 *
 */
public class GradingStudents {

    /**
     * @param args
     */
    public static void main(String[] args) {
	// TODO Auto-generated method stub
	int[] g = new int[]{73,	67, 38,	33};
	
	for(int grade : g)
	{
	    
		
	    int diff = grade % 5;
	    if(grade >= 38 && ((grade+5 - diff)-grade) < 3)
		System.out.println((grade+5-diff));
	    else
		System.out.println(grade);
	}
    }

}
