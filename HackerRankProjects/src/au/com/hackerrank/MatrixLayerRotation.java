/**
 * 
 */
package au.com.hackerrank;

import java.util.Arrays;
import java.util.Scanner;

/**
 * @author vnayak
 *
 */
public class MatrixLayerRotation {

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		// Scanner in = new Scanner(System.in);
		// int r = in.nextInt();
		// int c = in.nextInt();
		// int rotation = in.nextInt();
		// int[][] array = new int[r][c];
		// for (int i = 0; i < r; i++) {
		// for (int j = 0; j < c; j++) {
		// array[i][j] = in.nextInt();
		// }
		// }

		int[][] mat = new int[][] { { 1, 2 }, { 3, 4 } };
		int rotation = 2;
		display(mat);
		int numOfRows = mat.length;
		int numOfCol = mat[0].length;

		int[][] result = new int[numOfRows][numOfCol];

		int current_min_r = 0;
		int current_min_c = 0;
		int current_max_r = numOfRows - 1;
		int current_max_c = numOfCol - 1;
		for (int i = 0; i < Math.min(numOfCol, numOfRows) / 2; i++) {
			readAndWrite(mat, current_min_r, current_min_c, current_max_r, current_max_c, result, rotation);
			current_min_c++;
			current_min_r++;
			current_max_c--;
			current_max_r--;
		}

		display(result);

	}

	private static void readAndWrite(int[][] mat, int current_min_r, int current_min_c, int current_max_r,
			int current_max_c, int[][] result, int rotation) {

		int[] temp = read(mat, current_min_r, current_min_c, current_max_r, current_max_c);

		temp = shift(temp, rotation);

		write(temp, result, current_min_r, current_min_c, current_max_r, current_max_c);
	}

	private static void write(int[] temp, int[][] result, int current_min_r, int current_min_c, int current_max_r, int current_max_c) {
	}

	private static int[] read(int[][] mat, int current_min_r, int current_min_c, int current_max_r, int current_max_c) {
		int[] temp = null;
		
		int i = current_min_r;
		int j = current_min_r;
		
		while(true)
		{
			
			
			
			break;
		}
		
		return temp;
	}

	private static int[] shift(int[] array, int num) {
		int[] temp = new int[num];

		for (int i = 0; i < num; i++)
			temp[i] = array[i];

		for (int i = 0; i < array.length; i++) {
			if (i + num < array.length) {
				array[i] = array[i + num];
			} else {
				for (int j = 0; j < temp.length; j++) {
					array[i] = temp[j];
					i++;
				}
				break;
			}
		}

		return array;
	}

	private static void display(int[][] array) {
		int row = array.length;
		int col = array[0].length;

		for (int i = 0; i < row; i++) {
			for (int j = 0; j < col; j++) {
				System.out.print(array[i][j]);
				if (j != col - 1) {
					System.out.print(" ");
				} else {
					System.out.print("\n");
				}
			}
		}
	}

}
