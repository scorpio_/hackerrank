/**
 * 
 */
package au.com.hackerrank;

/**
 * @author Aakira
 *
 */
public class TimeConversion {

    /**
     * @param args
     */
    public static void main(String[] args) {
	// String time ="12:00:00AM";
	String time = "06:40:03AM";
	// String time ="1:00:00PM";

	if (time.equals("12:00:00AM")) {
	    System.out.println("00:00:00");
	} else if (time.equals("12:00:00PM")) {
	    System.out.println("12:00:00PM");
	} else if (time.contains("PM")) {
	    time = time.replace("PM", "");

	    String[] parts = time.split(":");

	    if (Integer.parseInt(parts[1]) < 60 && parts[0].equals("12")) {
		System.out.println(parts[0] + ":" + parts[1] + ":" + parts[2]);
	    } else {
		System.out.println(Integer.parseInt(parts[0]) + 12 + ":" + parts[1] + ":" + parts[2]);
	    }
	} else if (time.contains("AM") && time.startsWith("12")) {
	    System.out.println((time.replace("12", "00")).replaceAll("AM", ""));
	}else 
	    System.out.println(time.replace("AM", ""));

    }

}
